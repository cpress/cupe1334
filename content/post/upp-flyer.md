---
title: UPP Update
date: 2019-02-13
categories:
- Pensions
- News
tags:
- UPP
thumbnail: "/img/upp-update.png"
description: this is is

---
## Are your pension bargaining rights being put at risk?

The University of Guelph has been actively involved in putting together a new, multi-employer “Jointly Sponsored Pension Plan” (JSPP), along with the University of Toronto and Queen’s. The university is now seeking the formal consent of its faculty association, unions, and non-unionized employees for the full-scale conversion of its existing pension plans into a proposed new plan – to be called the “University Pension Plan” (UPP).

CUPE has been formally excluded from the process of developing this plan since July 2017 when we refused to accept an employer proposal that we give up important early retirement rights (a move to a minimum age 62 for no-penalty early retirement). While that concern was addressed in October of 2018, the process has now accelerated quickly. We are now seeing a rushed process to obtain ratification votes to support that consent from the various unions and associations.

As it stands, CUPE Locals 1334 and 3913 are not prepared to support the proposed plan as it is currently structured, nor to give our consent to the university to “convert” our secure, collectively- bargained plan – the Retirement Plan – into the new, non-bargained UPP. We recently obtained a legal opinion with respect to this proposed plan, and this opinion has given us reason to be very seriously concerned about the implications of this new plan for our members.

## CUPE’s concerns

For these reasons, we have decided to alert our members and all members of the University of Guelph “Retirement Plan” (including pension-eligible employees belonging to USW, OSSTF, OPSEU, Unifor, the Food Services Employees Association, as well as non-unionized employees and retirees) about our concerns. They include the following:

1. The employer-proposed Memorandum of Agreement that would bind unionized employees to participate in the UPP would also eliminate our collective bargaining rights in relation to almost all pension issues. Even on key issues of concern frequently negotiated and improved within other big Ontario pension plans (like OMERS), such as plan eligibility (for part-time workers, sessional lecturers, etc.), collective bargaining would not be permitted under this plan, and the rights to grieve and to strike on pensions will be lost. CUPE does not concede these bargaining rights.
2. The allocation of seats and votes on the ‘labour side’ of the key decision-making Sponsor Board will include a permanent three-out-of-six seat and vote share for the plan’s faculty associations, with no mechanism for adjustment to that allocation in the future, even if the faculty groups’ relative share of the plan’s membership were to decline significantly.
3. The key terms of the proposed UPP, including the benefit and contribution levels for the plan, are outlined in a document (“Milestones Agreement”) that could be modified at any time by the parties, even after consent votes are taken. There is no guarantee that the plan terms will be maintained between now and the date that the proposed plan is officially launched (in July 2021).

CUPE Locals 1334 and 3913 will be working to ensure that our concerns are addressed before agreeing to join this new plan. To do this, we plan to mobilize our members and as many other members of the Retirement Plan as possible to secure an appropriate framework for pension provision that does not strip us of our pension bargaining rights. We encourage everyone to ask questions about the issues raised above (and any others you may have) and to demand detailed answers before making any decisions.

CUPE is reviewing all of its options with respect to the university’s proposed new plan, and we will be communicating further on this issue in the near future.

For more information, please contact:  
Janice Folk-Dawson,  
President, CUPE Local 1334  
president@cupe1334.org