---
title: "Required Support Staff Policy (Formerly Essential Services)"
date: 2019-02-26
categories: ["News"]
tags: ["University Policy"]
thumbnail: "/img/report-process.png"
description: "this is is"

---

This is a University of Guelph policy applying to all Physical Resources employees.

[Please download the full policy here.](/files/required_support_services_policy_march_01_2018.pdf)

## Purpose

In the event of hazardous weather conditions (snow storm or freezing rain) or any other emergency situation,
the University may suspend operations and non-essential services in whole or in part. However, the
University of Guelph, Human Resources policy 512 Hazardous Weather/Emergency Closing Procedures, All
Staff, identifies Physical Resources as a provider of essential services for the main campus. This policy
serves to ensure Physical Resources employees performing work required to provide the delivery of
essential services (required support services as defined within this Physical Resources policy) are aware of
both their designation and responsibilities as per this policy.

## Scope

This policy applies to all Physical Resources employees.

## References

- University of Guelph, Human Resources policy 512 Hazardous Weather/Emergency Closing
Procedures, All Staff

- All applicable collective agreements
