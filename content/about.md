---
title: About
date: 2019-05-01
authorbox: true
categories:
- Resources
sidebar: true
menu:
  main: {}

---

> CUPE 1334 Original Logo
>
> ![](/img/avatar-old.jpg "Original Logo")

## CUPE Local 1334

CUPE 1334 represents Trades, Maintenance and Service workers on the main Campus at the University of Guelph.  The Local received it’s Charter on July 20th, 1971.

The diversity of job classifications in the main unit includes drivers, material handlers, custodians, ground keepers, stores keepers, fire prevention officers, residence porters, building mechanics and trades.

In 2010, the local organized a group of Part Time library workers who now are Unit 1 of the Local.  This unit represents library workers in stack maintenance, accessibility and the annex.

Both units have just successfully negotiated collective agreements with 2019 expiry dates.

Along with ensuring members are represented on university convened committees, CUPE 1334 participates with the Guelph & District Labour Council and is active on the CUPE Ontario University Workers Coordinating Committee (OUWCC).

General Membership Meetings (GMM) are held the 3rd Tuesday of the month with the exceptions of July, August and December.  The GMM’s start at 7:30pm and are held in room 442 of the University Center.

## 1334 Contact information

### Mail

> CUPE 1334  
> 25 University Ave East  
> Guelph, Ontario, N1G 1M8

### Telephone and Email

* Union Office (answering machine): 519-836-5470
* Union Office: Ext: 58536
* Fax: 519-763-0810
* Phone: 519-836-5470
* Email: comms@cupe1334.org

### 1334 Classifications

In 2010, the local organized a group of Part Time library workers who now are Unit 1 of the Local.  This unit represents library workers in stack maintenance, accessibility and the annex.

We represent the Maintenance, Trades and Service personnel at the University of Guelph.

This includes:

* Custodians
* Grounds
* Building Mechanics
* Electricians
* Vehicle Services
* Carpenters
* Plumbers
* Residence Porters
* Fire Prevention Officers
* Parking Enforcement Officers
* Locksmiths
* Storeskeepers
* Pest Control

## CUPE at Guelph

There are 3 C.U.P.E. locals at the University of Guelph

### 1281

* is a composite local with members on university campuses across the province.
* members of 1281 at Guelph work at OPIRG. CFRU Radio, Center for Gender Empowerment and as local staff for

### 3913

* represents Graduate Teaching Assistants, Under-Grad Teaching Assistants, and Sessional Lecturers.
* Contact Info: questions@cupe3913.on.ca.

### 1334

* represents Trades, Maintenance and Service Workers and Unit 1 represents Part Time Library Workers.
* Contact Info: 519-836-5470
