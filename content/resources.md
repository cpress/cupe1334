---
title: Resources
date: 2019-05-01
linktitle: Resources
categories: ["Resources"]
menu:
  main:
    name: Resources
    weight: 10
---

## Collective Agreements

If you have questions about the contract, please get in touch with your steward or one of the executive.

Click on the link below to go to the Collective Agreement:

https://www.uoguelph.ca/hr/hr-services-staff-relations/employee-groups-agreements


## By Laws


- [CUPE 1334 By-Laws](/files/CUPE-1334-By-Laws.pdf)

## Committees 

### Physical Resources – Health and Safety:

- Worker Co-Chair:   Linda French

- Laura Maclure

- Nanci Morley

- Dan Brown

 

### Student Housing Services – Health and Safety

- Jeff Wilson

- Curtis Holmes

### Job Evaluation – Rating:

- Scott Reynolds

- Nick Gielan

- Laura Maclure

- Brian Gorman (Alternate)

- Dan Brown (Alternate)

- Vicki Lusignan (Alternate)

### Job Evaluation – Steering:

- Janice Folk-Dawson

- Nanci Morley

- Darrin Chasty

### Parking
- Darrin Chasty

### Employment Equity

- Janice Folk-Dawson
- Alternate: Laura Maclure

### Human Rights Advisory Group

- Janice Folk-Dawson
- Alternate: Laura Maclure

### Code of Ethical Purchasing
- Janice Folk-Dawson
- Alternate: Laura Maclure
